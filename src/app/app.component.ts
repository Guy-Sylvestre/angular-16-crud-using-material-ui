import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { ApiService } from './services/api.service';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatSort, MatSortModule} from '@angular/material/sort';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'angular16-crud';
    displayedColumns: string[] = ['productName', 'category', 'data', 'freshness', 'price', 'comment', 'action'];
    dataSource!: MatTableDataSource<any>;

    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;

    constructor(private dialog: MatDialog, private api: ApiService) { }

    ngOnInit(): void {
        this.getAllProducts();
    }

    openDialog() {
        this.dialog.open(DialogComponent, {
            width: '30%'
        }).afterClosed().subscribe(value => {
            if (value == 'save') {
                this.getAllProducts();
            }
        })
    }

    getAllProducts() {
        this.api.getProduct().subscribe({
            next: (response) => {
                // console.log(response);
                this.dataSource = new MatTableDataSource(response);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            },
            error: (error) => {
                alert("Error while fetching the Recirds !!")
            }
        })
    }

    editProducts(row: any){
        this.dialog.open(DialogComponent, {
            width: '30%',
            data: row,
        }).afterClosed().subscribe(value => {
            if (value == 'update') {
                this.getAllProducts();
            }
        })
    }

    deleteProduct(id: number){
        this.api.deleteProduct(id).subscribe({
            next: (response) => {
                alert("Product Deleted Successfully");
                this.getAllProducts();
            },
            error: (err) => {
                alert("Error while deleting the product !!")
            },
        })
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }
}
